;; -*- lexical-binding: t -*-


;;;; Load our local utils before anything else ;;;;

(add-to-list 'load-path (expand-file-name "local" user-emacs-directory))
(require 'util)

;;;;


;;;; Install/Uninstall packages ;;;;

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(set-installed-packages 'corfu
                        'delight
                        'dockerfile-mode
                        'evil
                        'evil-collection
                        'exec-path-from-shell
                        'magit
                        'nyan-mode
                        'rust-mode
                        'vertico
                        'which-key)

;;;;


;;;; Misc/Core Settings ;;;;

;; When using built-in customize options, put the results into custom.el
;; instead of init.el. Then load that file from here.
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file 'noerror)

;; Turn off periodic auto-save (#foo# files).
(setq auto-save-default nil)

;; Turn off backup files (foo~ files).
(setq make-backup-files nil)
;; If we enable backups, the default approach breaks hard links.
;(setq backup-by-copying t)

;; On macOS, sync Emacs's exec path with OS env
(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))

;; Double-spacing is not considered proper/necessary anymore.
(setq sentence-end-double-space nil)

;; Automatically offer to reload file if it changed on disk.
(global-auto-revert-mode)
;;;;


;;;; UI Stuff ;;;;

;; Don't resize the frame just because fonts or toolbars change.
(setq frame-inhibit-implied-resize t)

;; Smooth scrolling has a bug with evil-mode. Try it out periodically.
;; Last tested with Emacs 29.1 and evil-mode 2023-09-19.
;(pixel-scroll-precision-mode 1)

;; Blinking cursors are annoying.
(blink-cursor-mode 0)

;; Disable alert noise, flashing titlebar, whatever.
(setq ring-bell-function #'ignore)

;; Configure lighters
(delight '((eldoc-mode nil "eldoc")
           (which-key-mode nil "which-key")))

;; Because it's awesome; duh!
(nyan-mode)
;;;;


;;;; Evil ;;;;

; These must be set for evil-collection
(setq evil-want-integration t  ; default is t already
      evil-want-keybinding nil)
(customize-set-variable 'evil-want-C-u-scroll t)
(customize-set-variable 'evil-want-C-u-delete t)
(customize-set-variable 'evil-undo-system 'undo-redo) ; requires Emacs 28+
(customize-set-variable 'evil-collection-want-unimpaired-p nil) 

(evil-mode t)
(evil-collection-init)

;; Vim treats _ character as part of a "word", but Emacs usually doesn't (mode-dependent).
(modify-syntax-entry ?_ "w")

;; Remap universal argument maps to M-u instead of C-u b/c we let vim have C-u
(bind-key "M-u" #'universal-argument)
(bind-key "M-u" #'universal-argument-more universal-argument-map)

;;;;


;;;; Dired ;;;;

(with-eval-after-load 'dired
  ;; Have dired sort directories first -- I don't know what -D does
  ;(setq dired-listing-switches "-alD --group-directories-first")
  ;; add -h ("h"uman readable sizes) to defaults
  (setq dired-listing-switches "-lah"))

;;;;


;;;; Which-key

(setq which-key-allow-evil-operators t
      which-key-show-operator-state-maps t)
(which-key-mode)

;;;;


;;;; Completions ;;;;

;; Allow things like completion while in the minibuffer.
(setq enable-recursive-minibuffers t)
(customize-set-variable 'read-extended-command-predicate #'command-completion-default-include-p)
(setq completion-category-overrides '((file (styles partial-completion))))
(setq completion-styles '(flex basic))

(vertico-mode)
(bind-key "TAB" #'minibuffer-complete vertico-map)
(customize-set-variable 'corfu-auto t)

;(use-package vertico
;  :config
;  (vertico-mode)
;  (define-key vertico-map (kbd "S-<backspace>") #'backward-kill-word))
  ;(defun my/vertico-minibuffer-complete ()
  ;  "Combine default minibuffer-complete behavior with vertico-insert as the fallback."
  ;  (interactive)
  ;  (unless (minibuffer-complete)
  ;    (vertico-insert)))
  ;; Override TAB behavior from vertico-insert to my custom behavior.
  ;(define-key vertico-map (kbd "TAB") #'my/vertico-minibuffer-complete))


;(use-package corfu
;  :hook (prog-mode . corfu-mode)
;  :custom
;  (corfu-auto t)
;  (corfu-quit-no-match 'separator))

;;;;


;;;; Projects and VCS ;;;;

;; project.el considers a directory with a root-marker file to be a project root.
;; This helps with monorepos that have multiple sub-projects.
(customize-set-variable 'project-vc-extra-root-markers '(".project.el"))

;; Disable the project.el support from magit b/c we do it ourselves
(setq magit-bind-magit-project-status nil)

;; Add magit to project.el
(with-eval-after-load 'project
  (bind-key "m" #'magit-project-status project-prefix-map)
  (add-to-list 'project-switch-commands '(magit-project-status "Magit") t))

;; Configure magit
(with-eval-after-load 'magit
  ;; Options are:
  ;; #'magit-display-buffer-traditional
  ;; #'magit-display-buffer-same-window-except-diff-v1
  ;; #'magit-display-buffer-fullframe-status-v1
  ;; #'magit-display-buffer-fullframe-status-topleft-v1
  ;; #'magit-display-buffer-fullcolumn-most-v1
  (setq magit-display-buffer-function
        #'magit-display-buffer-fullframe-status-v1))

;;;;


;;;; Programming languages/modes ;;;;

;; Shared/common prog-mode settings
(defun my/prog-mode-settings ()
  ;; No tabs for indent 
  (setq-local indent-tabs-mode nil)
  ;; Backspace deletes whole indent instead of single spaces
  (setq-local backward-delete-char-untabify-method 'hungry))

(add-hook 'prog-mode-hook #'display-line-numbers-mode)
(add-hook 'prog-mode-hook #'column-number-mode)
(add-hook 'prog-mode-hook #'show-paren-mode)
(add-hook 'prog-mode-hook #'corfu-mode)
(add-hook 'prog-mode-hook #'my/prog-mode-settings)

(setq eglot-autoshutdown t)
;; 0 => don't block while waiting for LSP connection.
(setq eglot-sync-connect 0)
;; todo - test this out
;(setq eglot-events-buffer-size 0)
;; Don't log jsonrpc events. May help eglot performance.
;(fset #'jsonrpc--log-event #'ignore)

;; Rust
(add-hook 'rust-mode-hook #'eglot-ensure)
(with-eval-after-load 'rust-mode
  (setq rust-format-on-save t))

;;;;


;;;; Key bindings ;;;;
(evil-set-leader 'motion (kbd "SPC"))
(evil-define-key 'motion 'global (kbd "<leader>w") #'evil-window-map)
(evil-define-key 'motion 'global (kbd "<leader>p") project-prefix-map)
(evil-ex-define-cmd "git" #'magit)
;;;;
