;;; package --- Useful functions -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(defun rotate-bottom-window-to-right ()
  "Move the bottom window to be a vertical split on the right side of frame."
  (interactive)
  (let* ((window (car (last (window-list nil 1))))
         (buffer (window-buffer window)))
    (delete-window window)
    (set-window-buffer (split-window (frame-root-window)
                                     nil
                                     'right)
                       buffer)))

(defun todo ()
  "Open my Emacs todo file."
  (interactive)
  (find-file "~/.emacs.d/todo.org"))

(defun init ()
  "Open my Emacs init file."
  (interactive)
  (find-file "~/.emacs.d/init.el"))

(defun my/evil-goto-definition-under-mouse (start-event)
  "Move the point under the mouse cursor, then call evil-goto-defintion.
START-EVENT is how Emacs does interactive clicky stuff."
  (interactive "e")
  (mouse-set-point start-event)
  (when (fboundp 'evil-goto-definition) (evil-goto-definition)))

(cl-flet ((always-yes (&rest _) t))
  (defun no-confirm (fun &rest args)
    "Apply FUN to ARGS, skipping user confirmations."
    (cl-letf (((symbol-function 'y-or-n-p) #'always-yes)
              ((symbol-function 'yes-or-no-p) #'always-yes))
      (apply fun args))))

(defun set-installed-packages (&rest packages)
  (setq package-selected-packages packages)
  (when (seq-some (lambda (package) (not (package-installed-p package))) packages)
    (unless package-archive-contents
      (package-refresh-contents))
    (package-install-selected-packages t))
  (no-confirm #'package-autoremove))

(provide 'util)
;;; util.el ends here
