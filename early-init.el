;;; early-init.el --- Early Init -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;; For nativecomp on macOS. Try to debug why we need this at all.
;(if (eq system-type 'darwin)
;    (setenv "LIBRARY_PATH"
;            "/opt/local/lib/gcc11:/opt/local/lib/libgcc:/opt/local/lib/gcc11/gcc/x86_64-apple-darwin21/11.2.0"))

(setq
 ;; Increase garbage collection threshold (100 MB)
 ;; This is recommended by the LSP people, but also probably good anyway
 ;gc-cons-threshold 100000000
 ;; No start-up screen
 inhibit-startup-screen t
 )

;;; UI stuff before the frame is actually loaded

(tool-bar-mode 0)
;; NOTE: Only built-in for Emacs 28+
(load-theme 'modus-operandi)

(provide early-init-file)
;;; early-init.el ends here
